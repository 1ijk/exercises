package main

import (
	"bytes"
	"testing"
)

func assertCorrect(t *testing.T, want string, got string) {
	t.Helper()
	if want != got {
		t.Errorf("wanted %s, got %s", want, got)
	}
}

func TestHello(t *testing.T) {
	t.Run("saying hello to people", func(t *testing.T) {
		var b bytes.Buffer
		Hello(&b, "Dave")
		assertCorrect(t, "Hello, Dave", b.String())
	})

	t.Run("saying hello anonymously", func(t *testing.T) {
		var b bytes.Buffer
		Hello(&b, "")
		assertCorrect(t, "Hello", b.String())
	})
}
