package main

import (
	"fmt"
	"io"
	"os"
)

const (
	greeting         = "Hello"
	personalGreeting = greeting + ", %s"
)

func Hello(out io.Writer, name string) {
	if name == "" {
		fmt.Fprint(out, greeting)
		return
	}
	fmt.Fprintf(out, personalGreeting, name)
}

func main() {
	var name string
	if len(os.Args) > 1 {
		name = os.Args[1]
	}

	Hello(os.Stdout, name)
	fmt.Print("\n")
}
